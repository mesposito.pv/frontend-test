/** @type {import('tailwindcss').Config} */
const defaultTheme = require("tailwindcss/defaultTheme");

export default {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        // added custom color for borders
        borderGray: "rgb(var(--border-gray) / 1)",
        // added custom color for email text
        email: "#6a7280",
      },
      // added Inter font family
      fontFamily: {
        inter: ['"Inter"', ...defaultTheme.fontFamily.sans],
      },
      // added custom variable to show a 1.5px border
      borderWidth: {
        "1-5": "1.5px",
      },
    },
  },
  plugins: [],
};
