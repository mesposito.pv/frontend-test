export default interface ItemType {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  profile_pic: string;
  children?: ItemType[];
}
